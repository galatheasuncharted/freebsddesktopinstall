__FreeBSDDesktopInstall__
==========================
This little programm will install a complete Desktop Envirement of your choise.

Currently full supported DE's are:

* XFCE4
* KDE5/Plasma5
* Gnome3
* Mate
* LXDE
* KDE4 (english only)

Avail but not supported:

* Cinnamon
* Blackbox

Listet but not able to install:

* Budgie

This program is also able to install and setup the graphics driver for you.

Supported graphics driver are:

* Nvidia
* AMD Radeon for Supported GPU's please see: https://wiki.freebsd.org/Graphics
* VirtualBox-OSE

Listet but not able to install:

* Intel

I'm currently work to implement the graphic driver.

You can clone this project by typing:

git clone https://bitbucket.org/galatheasuncharted/freebsddesktopinstall.git

You can build the program by typing:

make

You can clean the source by typing:

make clean

After you build the program you can run it by typing:

./FreeBSDDesktopInstaller