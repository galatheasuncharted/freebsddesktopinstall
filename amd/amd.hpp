/*
 * amd.hpp
 *
 *  Created on: 07.11.2017
 *      Author: gala
 */

#ifndef AMD_HPP_
#define AMD_HPP_

#include <iostream>

class amd {
public:
	amd();
	virtual ~amd();

    void createBootCfg();
    void createRC();
    void createXORG();
};

#endif /* AMD_HPP_ */
