/*
 * amd.cpp
 *
 *  Created on: 07.11.2017
 *      Author: gala
 */

#include "amd.hpp"

amd::amd() {
	// TODO Auto-generated constructor stub

}

amd::~amd() {
	// TODO Auto-generated destructor stub
}
void amd::createBootCfg() {
    /**
     * kern.vty=vt
     * linux_enable="YES"
     */
    std::string cmd = "echo 'kern.vty=vt' >> /boot/loader.conf";
    std::system(cmd.c_str());
    cmd = "echo 'linux_enable=\"yes\"' >> /boot/loader.conf";
    std::system(cmd.c_str());
}
void amd::createRC() {
    /**
     * kld_list="radeonkms"
     */
    std::string cmd = "echo 'kld_list=\"radeonkms\"' >> /etc/rc.conf";
    std::system(cmd.c_str());
}
void amd::createXORG() {
    /**
     * Section "Device"
     *      Identifier  "Card0"
     *      Driver      "radeon"
     * EndSection
     */
    std::string cmd = "mkdir -p /usr/local/etc/X11/xorg.conf.d/";
    std::system(cmd.c_str());
    cmd = "echo 'Section \"Device\"' >> /usr/local/etc/X11/xorg.conf.d/driver-radeon.conf";
    std::system(cmd.c_str());
    cmd = "echo '      Identifier  \"Card0\"' >> /usr/local/etc/X11/xorg.conf.d/driver-radeon.conf";
    std::system(cmd.c_str());
    cmd = "echo '      Driver      \"radeon\"' >> /usr/local/etc/X11/xorg.conf.d/driver-radeon.conf";
    std::system(cmd.c_str());
    cmd = "echo 'EndSection' >> /usr/local/etc/X11/xorg.conf.d/driver-radeon.conf";
    std::system(cmd.c_str());
}
