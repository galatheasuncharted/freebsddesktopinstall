/*
 * matrox.hpp
 *
 *  Created on: 08.11.2017
 *      Author: gala
 */

#ifndef MATROX_MATROX_HPP_
#define MATROX_MATROX_HPP_

#include <iostream>

class matrox {
public:
	matrox();
	virtual ~matrox();
    void createBootCfg();
    void createRC();
    void createXORG();
};

#endif /* MATROX_MATROX_HPP_ */
