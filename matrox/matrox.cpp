/*
 * matrox.cpp
 *
 *  Created on: 08.11.2017
 *      Author: gala
 */

#include "matrox.hpp"

matrox::matrox() {
	// TODO Auto-generated constructor stub

}

matrox::~matrox() {
	// TODO Auto-generated destructor stub
}

void matrox::createBootCfg() {
    /**
     * kern.vty=vt
     * linux_enable="YES"
     */
    std::string cmd = "echo 'kern.vty=vt' >> /boot/loader.conf";
    std::system(cmd.c_str());
    cmd = "echo 'linux_enable=\"yes\"' >> /boot/loader.conf";
    std::system(cmd.c_str());
}
void matrox::createRC() {
    /**
     * kld_list=""
     */
    //std::string cmd = "echo 'kld_list=\"\"' >> /etc/rc.conf";
    //std::system(cmd.c_str());
}
void matrox::createXORG() {
    /**
     * Section "Device"
     *      Identifier  "Card0"
     *      Driver      "mga"
     * EndSection
     */
    std::string cmd = "mkdir -p /usr/local/etc/X11/xorg.conf.d/";
    std::system(cmd.c_str());
    cmd = "echo 'Section \"Device\"' >> /usr/local/etc/X11/xorg.conf.d/driver-mga.conf";
    std::system(cmd.c_str());
    cmd = "echo '      Identifier  \"Card0\"' >> /usr/local/etc/X11/xorg.conf.d/driver-mga.conf";
    std::system(cmd.c_str());
    cmd = "echo '      Driver      \"mga\"' >> /usr/local/etc/X11/xorg.conf.d/driver-mga.conf";
    std::system(cmd.c_str());
    cmd = "echo 'EndSection' >> /usr/local/etc/X11/xorg.conf.d/driver-mga.conf";
    std::system(cmd.c_str());
}
