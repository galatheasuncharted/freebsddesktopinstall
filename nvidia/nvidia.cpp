/*
 * nvidia.cpp
 *
 *   Created on: 1.11.2017
 *       Author: Galatheas Uncharted
 * Project Name: FreeBSDDesktop Installer
 * Copyright (C) 2017  FreeBSDTutorials.org & Galatheas Uncharted
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "nvidia.hpp"

nvidia::nvidia() {
}

nvidia::~nvidia() {
}
void nvidia::createBootCfg() {
    /**
     * nvidia_load="yes"
     * linux_enable="YES"
     */
    std::string cmd = "echo 'nvidia_load=\"yes\"' >> /boot/loader.conf";
    std::system(cmd.c_str());
    cmd = "echo 'linux_enable=\"yes\"' >> /boot/loader.conf";
    std::system(cmd.c_str());
}
void nvidia::createRC() {
    /**
     * kld_list="nvidia nvidia-modeset"
     */
    std::string cmd = "echo 'kld_list=\"nvidia nvidia-modeset\"' >> /etc/rc.conf";
    std::system(cmd.c_str());
}
void nvidia::createXORG() {
    /**
     * Section "Device"
     *      Identifier  "Card0"
     *      Driver      "nvidia"
     * EndSection
     */
    std::string cmd = "mkdir -p /usr/local/etc/X11/xorg.conf.d/";
    std::system(cmd.c_str());
    cmd = "echo 'Section \"Device\"' >> /usr/local/etc/X11/xorg.conf.d/driver-nvidia.conf";
    std::system(cmd.c_str());
    cmd = "echo '      Identifier  \"Card0\"' >> /usr/local/etc/X11/xorg.conf.d/driver-nvidia.conf";
    std::system(cmd.c_str());
    cmd = "echo '      Driver      \"nvidia\"' >> /usr/local/etc/X11/xorg.conf.d/driver-nvidia.conf";
    std::system(cmd.c_str());
    cmd = "echo 'EndSection' >> /usr/local/etc/X11/xorg.conf.d/driver-nvidia.conf";
    std::system(cmd.c_str());
}
