/*
 * installer.hpp
 *
 *  Created on: 02.11.2017
 *       Author: Galatheas Uncharted
 * Project Name: FreeBSDDesktop Installer
 * Copyright (C) 2017  FreeBSDTutorials.org & Galatheas Uncharted
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <sstream>
#include "../preinstall/preinstall.hpp"
#include "../postinstall/postinstall.hpp"
#include "../utils/utils.hpp"

#ifndef INSTALLER_INSTALLER_HPP_
#define INSTALLER_INSTALLER_HPP_

class installer {
public:
	installer();
	virtual ~installer();

	bool run();

	bool makeInstall(const std::string &packageList);
	bool preTasks();
	bool postTasks();
	bool performInstall();

	/**
	 * Every single task
	 */
	void setDesktopPackages(int &desktop);
	void setGraphic();
	void setBrowser();
	void setMultimedia();
	void setOffice();
	void setSysUtils();

private:
	std::stringstream cmd;
	preinstall pre;
	postinstall post;
	utils u;

	int i = 1; 					// this is for every for loops
	int browser = -1;			// just initial integer Var
	int desktop = -1;			// "
	int graphic = -1;			// "
};

#endif /* INSTALLER_INSTALLER_HPP_ */
