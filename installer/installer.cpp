/*
 * installer.cpp
 *
 *  Created on: 02.11.2017
 *       Author: Galatheas Uncharted
 * Project Name: FreeBSDDesktop Installer
 * Copyright (C) 2017  FreeBSDTutorials.org & Galatheas Uncharted
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include "installer.hpp"

installer::installer() {
	this->cmd << "pkg install xorg ";
}

installer::~installer() {
	u.printByeBye();
}

bool installer::run() {
	u.printWelcome();
	if(preTasks()) {
		return true;
	}
	return false;
}
bool installer::preTasks() {
	/**
	 * Desktop selection
	 */
	this->desktop = u.askInt("Desktop Envirement", pre.getDesktop());	//todo Check for int 1 to 9 in askInt()
	std::cout << "Ok we install the " << pre.getDesktopName(desktop) << " Desktop for you!" << std::endl;
	setDesktopPackages(desktop);

	/**
	 * Graphic Card Selection
	 */
	pre.setGraphic(u.askBool("Graphic Driver", pre.getGraphic()));
	if(pre.installGraphic()) {
		setGraphic();
	}

	pre.setBrowser(u.askBool("Browser", pre.getBrowser()));

	if(pre.installBrowser()) {
		setBrowser();
	}

	/**
	 * Multimedia Packages
	 */
	std::cout << "Select Multimedia Package" << std::endl;
	u.printVector(pre.getMultimedia());
	pre.setMultimedia(u.askBool("Multimedia Software", pre.getMultimedia()));
	if(pre.installMultimedia()){
		setMultimedia();
	}

	/**
	 * Office Packages
	 */
	std::cout << "Select Office Package" << std::endl;
	u.printVector(pre.getOffice());
	pre.setOffice(u.askBool("An Office package", pre.getOffice()));
	if(pre.installOffice()){
		setOffice();
	}

	/**
	 * System Utilities
	 */
	std::cout << "Select System Utilitie Package" << std::endl;
	u.printVector(pre.getSysutils());
	pre.setSysUtils(u.askBool("System Utilities", pre.getSysutils()));
	if(pre.installSysUtils()){
		setSysUtils();
	}

	/**
	 * start Installation
	 */
	if(performInstall()) {

		/**
		 * Post installation tasks
		 */
		if(postTasks()) {
			return true;
		}

	}

	return false;
}
bool installer::postTasks() {
	/**
	 * Start Post installation tasks
	 */

	std::cout << "Ok Installation process is done.\nLets Setup the Configuration Files!\nDO NOT INTERRUPT THIS PROCESS!!" << std::endl;

	// Generate FSTAB
	post.genFsTab();

	// Generate SUDOERS
	post.genSudo();	// todo make implementation

	// System Profile Languages Exports
	post.genSysProfile();

	/*
	 * make RC.CONF defaults
	 */
	std::string graphic_card = u.toLower(pre.getGraphicName(graphic));
	post.genRC();
	post.genRCGraphic(graphic_card);

	/**
	 * Switch case Desktop
	 */

	switch(desktop) {
		case 1:						// XFCE4
			post.genRCXFCE();
			post.genPolicy();
			post.genXinit();
			post.genXsesseion();
			break;
		case 2:						// Gnome3
			post.genRCGnome();
			break;
		case 3:						// KDE4
			post.gentRCKDE();
			break;
		case 4:						// Cinnamon
			post.genRCCinnamon();
				break;
		case 5:						// Mate
			post.genRCMate();
			post.gendXinitMate();
			break;
		case 6:						// Budgie
			post.genRCBudgie();
			break;
		case 7:						// BlackBox
			post.genRCBlackBox();
			break;
		case 8:						// LXDE
			post.genRCLXDE();
			break;
		case 9:
			post.genPlasma();		// KDE/Plasma 5
		}

	post.genUserProfile();
	post.getUserShell();
	post.makeGermanKbd();
	return true;
}

bool installer::performInstall() {
	/**
	 * Start installation
	 */
	std::cout << "We start now the Installation process......" << std::endl;
	std::cout << "Please Wait and DO NOT INTERRUPT THIS PROCESS!!!!!" << std::endl;
	if(std::system(this->cmd.str().c_str()) != -1) {
		return true;
	}
	return false;
}

void installer::setDesktopPackages(int &desktop) {
	switch(desktop) {
		/**
		 * Fall through XFCE4 Cinnamon Mate Blackbox
		 */
		case 1:
		case 4:
		case 5:
		case 7:
			this->cmd << pre.getDesktopPackage(desktop) << " ";
			this->cmd << u.toLower(pre.getDisplayName(i)) << " ";
			break;
		case 6:
			std::cout << "Budgie is currently not Supported. There're no pkg Packages to install it." << std::endl;
			exit(0);
		case 8:
			/*
			 * TODO Change from i to a fixed value
			 */
			i = 5;
			cmd << u.toLower(pre.getDisplayName(i)) << " ";
			i = 1;
			break;
		case 2:
			cmd << pre.getDesktopPackage(desktop) << " ";
			cmd << u.toLower(pre.getDisplayName(desktop)) << " ";
			break;
		case 3:
			cmd << pre.getDesktopPackage(desktop) << " ";
			break;
		case 9:
			cmd << pre.getDesktopPackage(desktop) << " ";
		}

}

void installer::setGraphic() {
	this->graphic = u.askInt("Graphic Driver", pre.getGraphic());
	std::cout << "Ok we install the " << pre.getGraphicName(this->graphic) << " graphic drivers for you!" << std::endl;

	switch(this->graphic) {
	case 1:
		cmd << "x11-drivers/xf86-video-ati" << " ";
		break;
	case 2:
		cmd << "x11-drivers/xf86-video-intel" << " ";
		break;
	case 3:
		cmd << "nvidia-driver nvidia-settings nvidia-xconfig ";
		std::system("kldload linux");
		break;
	case 4:
		cmd << u.toLower(pre.getGraphicName(this->graphic)) << " ";
		break;
	case 5:
		cmd << "xf86-video-mga ";
		break;
	}
}

void installer::setBrowser() {
	this->browser = u.askInt("Browser", pre.getBrowser());
	std::cout << "Ok we install the " << pre.getBrowserName(this->browser) << " Webbrowser for you!" << std::endl;
	cmd << u.toLower(pre.getBrowserName(this->browser)) << " ";
}

void installer::setMultimedia() {
	std::cout << "Ok we will install the Multimedia Package" << std::endl;
	for (auto &it : pre.getMultimedia()) {
		cmd << u.toLower(it) << " ";
	}
}

void installer::setOffice() {
	std::cout << "Ok we will install the Office Package" << std::endl;
	for (auto &it : pre.getOffice()) {
		cmd << u.toLower(it) << " ";
	}
	cmd << "de-libreoffice hunspell de-hunspell ";
}

void installer::setSysUtils() {
	std::cout << "Ok we will install System Utilities" << std::endl;
	for (auto &it : pre.getSysutils()) {
		cmd << u.toLower(it) << " ";
	}
}
