/*
 * utils.cpp
 *
 *   Created on: 25.10.2017
 *       Author: Galatheas Uncharted
 * Project Name: FreeBSDDesktop Installer
 * Copyright (C) 2017  FreeBSDTutorials.org & Galatheas Uncharted
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.hpp"

utils::utils() {


}

utils::~utils() {

}

bool utils::askBool(const std::string &operation, const std::vector<std::string> &vector) {
	char a;
	do
	{
		std::cout << "Would you like to install " << operation << "? (Y/n) ";
		a='y';
	    std::cin >> a;
	}
	while( !std::cin.fail() && std::tolower(a)!='y' && std::tolower(a)!='n' && std::tolower(a)!='\n');
	switch(a) {
		case 'y':
			return true;
		case 'n':
			return false;
		default:
			return true;
	}

}
int utils::askInt(const std::string &operation, const std::vector<std::string> &vector) {
	std::cout << "Category " << operation << std::endl;
	int i = 1;
	for(const auto &it : vector) {
		std::cout << i << ". " << it << std::endl;
		i++;
	}
		unsigned int a = 1;
		std::cout << "Please enter a Number for " << operation << "? [1]: ";
		std::cin >> a;

		if(a < 1 || a > vector.size()) {
			std::cout << "Wrong input. Only use 1 to " << vector.size() << std::endl;
			std::cin.ignore(1000,'\n');
			askInt(operation, vector);
		}
		return a;
}

std::string utils::toLower(std::string string) {
	for(unsigned int i = 0; i < string.length(); ++i) {
	    string[i] = tolower(string[i]);
	}
	return string;

}

void utils::printVector(const std::vector<std::string> &vector) {
	int i = 1;
	for (const auto &it : vector) {
		std::cout << i << ". " << it << std::endl;
		i++;
	}

}

void utils::printWelcome() {
	std::cout << "Wellcome to FreeBSDDesktopInstall v" << this->version_ << std::endl;
	std::cout << "With this little Program you are able to install a FreeBSD Desktop of your choise" << std::endl;
	std::cout << "Just follow the steps." << std::endl;
}

void utils::printByeBye() {
	std::cout << "Ok looks like we are finished." << std::endl;
	std::cout << "If you're not familer how to start services, just restart by typing reboot." << std::endl;
	std::cout << "Else you can just start dbus hald vboxguest vboxservice and your displaymanager (eg. lightdm for XFCE4)" << std::endl;
	std::cout << "Enjoy your new FreeBSD Desktop Computer" << std::endl;
	std::cout << "Have a lot of fun" << std::endl;
	std::cout << "Greetings" << std::endl;
	std::cout << "Galatheas Uncharted" << std::endl;
}
