/*
 * utils.hpp
 *
 *   Created on: 25.10.2017
 *       Author: Galatheas Uncharted
 * Project Name: FreeBSDDesktop Installer
 * Copyright (C) 2017  FreeBSDTutorials.org & Galatheas Uncharted
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_UTILS_HPP_
#define UTILS_UTILS_HPP_

#include <iostream>
#include <limits>
#include <vector>
#include <string>

class utils {
public:
	utils();
	virtual ~utils();

	bool askBool(const std::string &operation, const std::vector<std::string> &vector);
	int askInt(const std::string &operation, const std::vector<std::string> &vector);

	std::string toLower(std::string string);

	std::string prepareInstall();
	void printVector(const std::vector<std::string> &vector);
	void printWelcome();
	void printByeBye();
private:
	std::string version_ = "0.10.6";
};

#endif /* UTILS_UTILS_HPP_ */
