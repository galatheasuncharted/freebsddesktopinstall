/*
 * postinstall.hpp
 *
 *   Created on: 26.10.2017
 *       Author: Galatheas Uncharted
 * Project Name: FreeBSDDesktop Installer
 * Copyright (C) 2017  FreeBSDTutorials.org & Galatheas Uncharted
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef POSTINSTALL_POSTINSTALL_HPP_
#define POSTINSTALL_POSTINSTALL_HPP_

#include <string>
#include <unistd.h>
#include <pwd.h>
//#include <dirent.h>
#include <sys/stat.h>
#include <fstream>
#include "../nvidia/nvidia.hpp"
#include "../amd/amd.hpp"
#include "../matrox/matrox.hpp"
#include "../intel/intel.hpp"

class postinstall {
public:
	postinstall();
	virtual ~postinstall();

	void genFsTab();
	void genSudo();

	/**
	 * All Desktops Display Manager and Graphic Cards
	 */
	void genRC();
	void genRCGraphic(std::string &graphic);
	void genRCXFCE();
	void genRCGnome();
	void gentRCKDE();
	void genRCMate();
	void genRCCinnamon();
	void genRCBudgie();
	void genRCBlackBox();
	void genRCLXDE();

	void genPlasma();
	void addRepository();
	void installPlasma();

	/**
	 * TODO Change everything to filestream
	 */


	/**
	 * blub
	 */
	void genPolicy();

	/**
	 * All User xinitrc files
	 */
	void genXinit();
	void gendXinitMate();



	void genXsesseion();
	void genSysProfile();
	void genUserProfile();
	void getUserShell();
	void makeGermanKbd();

	template<size_t N>
	void writeFile(const std::string &path, const std::string (&lines)[N]);

private:
	int id = 1001;
	struct passwd *mypasswd;
};

#endif /* POSTINSTALL_POSTINSTALL_HPP_ */
