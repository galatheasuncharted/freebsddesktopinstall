/*
 * postinstall.cpp
 *
 *   Created on: 26.10.2017
 *       Author: Galatheas Uncharted
 * Project Name: FreeBSDDesktop Installer
 * Copyright (C) 2017  FreeBSDTutorials.org & Galatheas Uncharted
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "postinstall.hpp"

postinstall::postinstall() {

	/**
	 * Get passwd vars for the fist User ID (1001)
	 */
	this->mypasswd = getpwuid(this->id);

}

postinstall::~postinstall() {

}

void postinstall::genFsTab() {
	std::fstream output("/etc/fstab", std::fstream::app);
	output << "proc                    /proc   procfs  rw              0       0" << std::endl;
	output.close();
}
void postinstall::genSudo() {

	std::string cmd = "sed -i -e 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g' /usr/local/etc/sudoers";
	std::system(cmd.c_str());

}
void postinstall::genRC() {
	std::string lines [] {
			"hald_enable=\"yes\"",
			"dbus_enable=\"yes\""
	};
	std::string path = "/etc/rc.conf";
	writeFile(path, lines);
}

void postinstall::genRCGraphic(std::string &graphic) {

	if(graphic == "virtualbox-ose-additions") {
		std::string lines [] {
				"vboxguest_enable=\"yes\"",
				"vboxservice_enable=\"yes\""
		};
		std::string path = "/etc/rc.conf";
		writeFile(path, lines);
	} else if (graphic == "amd") {
		amd radeon;
		radeon.createBootCfg();
		radeon.createRC();
		radeon.createXORG();

	} else if (graphic == "intel") {
		intel i;
		i.createBootCfg();
		i.createRC();
		i.createXORG();

	}else if (graphic == "nvidia") {
		nvidia n;
        n.createBootCfg();
        n.createRC();
        n.createXORG();

	} else if(graphic == "matrox") {
		matrox m;
		m.createBootCfg();
		m.createRC();
		m.createXORG();
	}
}
void postinstall::genRCXFCE() {
	std::fstream output("/etc/rc.conf", std::fstream::app);
	output << "lightdm_enable=\"yes\"" << std::endl;
	output.close();

	/**
	 * Code is temporary
	 * this code set up a default
	 * theme for XFCE4
	 */
	/*
	std::system("fetch --no-verify-peer http://freebsdtutorials.org/bsd_install/InstallerPackages.tar.bz2 >> /dev/null");
	std::system("mv InstallerPackages.tar.bz2 /tmp/");
	std::system("cd /tmp");
	std::system("tar -xjvf InstallerPackages.tar.bz2");
	std::string dir = this->mypasswd->pw_dir;
	std::string path = dir + "/.config/";
	mkdir(path.c_str(), 655);
	std::string copy = "cp -R .xfce4 " + path;
	std::system(copy.c_str());
	copy = "cp Pictures/freebsd_wallpaper.jpg /usr/local/share/backgrounds/";
	std::system(copy.c_str());
	copy = "cp Pictures/freebsd_icon.png /usr/local/share/pixmaps/";
	std::system(copy.c_str());
	std::system("rm -rf InstallerPackages.tar.bz2 Pictures xfce4");
	std::system("cd");
	*/
}
void postinstall::genRCGnome() {
	std::string lines [] {
		"gdm_enable=\"yes\"",
		"gnome_enable=\"yes\""
	};
	std::string path = "/etc/rc.conf";
	writeFile(path, lines);
}
void postinstall::gentRCKDE() {
	std::fstream output("/etc/rc.conf", std::fstream::app);
	output << "kdm4_enable=\"yes\"" << std::endl;
	output.close();
}
void postinstall::genRCMate() {
	genRCXFCE();
}
void postinstall::genRCCinnamon() {
	genRCXFCE();
}
void postinstall::genRCBudgie() {
	genRCXFCE();
}
void postinstall::genRCBlackBox() {
	genRCXFCE();
}
void postinstall::genRCLXDE() {

	std::string dir = mypasswd->pw_dir;

	/**
	 * Make rc.conf
	 */
	std::fstream output("/etc/rc.conf", std::fstream::app);
	output << "xdm_enable=\"yes\"" << std::endl;
	output.close();
	std::string cmd;

	/**
	 * Make the .xinitrc
	 */
	std::string lines[] {
			"#!/bin/sh",
			"exec startlxdm"
	};
	std::string path = dir + "./xinitrc";
	writeFile(path, lines);
	chown(path.c_str(), this->mypasswd->pw_uid, this->mypasswd->pw_gid);

	/**
	 * Make the .xsession
	 */
	std::string lines2[] {
			"#!/bin/sh",
			"exec startlxde"
	};
	std::string path2 = dir + "./xsession";
	writeFile(path2, lines2);
	chown(path.c_str(), this->mypasswd->pw_uid, this->mypasswd->pw_gid);

}
void postinstall::genPolicy() {
	std::string lines[] {
			"polkit.addRule(function (action, subject) {",
			"  if ((action.id == \"org.freedesktop.consolekit.system.restart\" ||",
			"      action.id == \"org.freedesktop.consolekit.system.stop\")",
			"      && subject.isInGroup(\"operator\")) {",
			"    return polkit.Result.YES;",
			"  }",
			"});"
	};
	std::string path = "/usr/local/etc/polkit-1/rules.d/10-restart.stop.rules";
	writeFile(path, lines);
}
void postinstall::genXinit() {
	std::string lines[] {
			"export LANG=de_DE.UTF-8",
			"export LC_CTYPE=de_DE.UTF-8",
			"export LC_COLLATE=de_DE.UTF-8",
			"export MM_CHARSET=UTF-8",
			"export LC_TYPE=de_DE.UTF-8",
			"export LC_MESSAGES=de_DE.UTF-8",
			"export LC_MONETARY=de_DE.UTF-8",
			"export LC_NUMERIC=de_DE.UTF-8",
			"export LC_TIME=de_DE.UTF-8",
			"export CHARSET=UTF-8",
			"export LC_ALL=de_DE.UTF-8",
			"",
			"exec /usr/local/bin/startxfce4 --with-ck-launch"
	};
	std::string dir = this->mypasswd->pw_dir;
	std::string path = dir + "/.xinitrc";
	writeFile(path, lines);
	chown(path.c_str(), mypasswd->pw_uid, mypasswd->pw_gid);

}

void postinstall::gendXinitMate() {
	std::string lines[] {
			"export LANG=de_DE.UTF-8",
			"export LC_CTYPE=de_DE.UTF-8",
			"export LC_COLLATE=de_DE.UTF-8",
			"export MM_CHARSET=UTF-8",
			"export LC_TYPE=de_DE.UTF-8",
			"export LC_MESSAGES=de_DE.UTF-8",
			"export LC_MONETARY=de_DE.UTF-8",
			"export LC_NUMERIC=de_DE.UTF-8",
			"export LC_TIME=de_DE.UTF-8",
			"export CHARSET=UTF-8",
			"export LC_ALL=de_DE.UTF-8",
			"",
			"exec ck-launch-session dbus-launch --exit-with-session mate-session"
	};
	std::string dir = this->mypasswd->pw_dir;
	std::string path = dir + "/.xinitrc";
	writeFile(path, lines);
	chown(path.c_str(), mypasswd->pw_uid, mypasswd->pw_gid);

}
void postinstall::genXsesseion() {
	std::string lines[] {
		"#!/bin/sh",
		"export LANG=de_DE.UTF-8",
		"export LC_CTYPE=de_DE.UTF-8",
		"export LC_COLLATE=de_DE.UTF-8",
		"export MM_CHARSET=UTF-8",
		"export LC_TYPE=de_DE.UTF-8",
		"export LC_MESSAGES=de_DE.UTF-8",
		"export LC_MONETARY=de_DE.UTF-8",
		"export LC_NUMERIC=de_DE.UTF-8",
		"export LC_TIME=de_DE.UTF-8",
		"export CHARSET=UTF-8",
		"export LC_ALL=de_DE.UTF-8",
		"exec /usr/local/bin/startxfce4 --with-ck-launch"
	};
	std::string dir = this->mypasswd->pw_dir;
	std::string path =  dir + "/.xsession";
	writeFile(path, lines);
	chown(path.c_str(), mypasswd->pw_uid, mypasswd->pw_gid);

}
void postinstall::genSysProfile() {
	std::string lines[] {
			"export LANG=de_DE.UTF-8",
			"export LC_CTYPE=de_DE.UTF-8",
			"export LC_COLLATE=de_DE.UTF-8",
			"export MM_CHARSET=UTF-8",
			"export LC_TYPE=de_DE.UTF-8",
			"export LC_MESSAGES=de_DE.UTF-8",
			"export L C_MONETARY=de_DE.UTF-8",
			"export LC_NUMERIC=de_DE.UTF-8",
			"export LC_TIME=de_DE.UTF-8",
			"export CHARSET=UTF-8",
			"export LC_ALL=de_DE.UTF-8"
	};

	std::string path = "/etc/profile";
	writeFile(path, lines);
}
void postinstall::genUserProfile() {
	std::string lines[] {
		"# $FreeBSD: releng/11.0/share/skel/dot.profile 278616 2015-02-12 05:35:00Z cperciva $",
		"#",
		"# .profile - Bourne Shell startup script for login shells",
		"#",
		"# see also sh(1), environ(7).",
		"#",
		","
		"# These are normally set through /etc/login.conf.  You may override them here",
		"# if wanted.",
		"PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin:$HOME/bin; export PATH",
		"# BLOCKSIZE=K;  export BLOCKSIZE",
		","
		"# Setting TERM is normally done through /etc/ttys.  Do only override",
		"# if you're sure that you'll never log in via telnet or xterm or a",
		"# serial line.",
		"# TERM=xterm;   export TERM",
		"",
		"EDITOR=nano;      export EDITOR",
		"PAGER=more;     export PAGER",
		"",
		"# set ENV to a file invoked each time sh is started for interactive use.",
		"ENV=$HOME/.cshrc; export ENV",
		"",
		"if [ -x /usr/bin/fortune ] ; then /usr/bin/fortune freebsd-tips ; fi",
		"",
		"export LANG=de_DE.UTF-8",
		"export LC_CTYPE=de_DE.UTF-8",
		"export LC_COLLATE=de_DE.UTF-8",
		"export MM_CHARSET=UTF-8",
		"export LC_TYPE=de_DE.UTF-8",
		"export LC_MESSAGES=de_DE.UTF-8",
		"export LC_MONETARY=de_DE.UTF-8",
		"export LC_NUMERIC=de_DE.UTF-8",
		"export LC_TIME=de_DE.UTF-8",
		"export CHARSET=UTF-8",
		"export LC_ALL=de_DE.UTF-8"
	};
	std::string dir = this->mypasswd->pw_dir;
	std::string path = dir + "/.profile";
	writeFile(path, lines);
	chown(path.c_str(), this->mypasswd->pw_uid, this->mypasswd->pw_gid);
}
void postinstall::getUserShell() {

	std::string lines[] {
		"# $FreeBSD: releng/11.0/share/skel/dot.cshrc 278616 2015-02-12 05:35:00Z cperciva $",
		"#",
		"# .cshrc - csh resource script, read at beginning of execution by each shell",
		"#",
		"# see also csh(1), environ(7).",
		"# more examples available at /usr/share/examples/csh/",
		"#",
		"",
		"alias h         history 25",
		"alias j         jobs -l",
		"alias la        ls -aF",
		"alias lf        ls -FA",
		"alias ll        ls -lAF",
		"alias ls        ls -G",
		"",
		"# These are normally set through /etc/login.conf.  You may override them here",
		"# if wanted.",
		"# set path = (/sbin /bin /usr/sbin /usr/bin /usr/local/sbin /usr/local/bin $HOME/bin)",
		"# setenv        BLOCKSIZE       K",
		"# A righteous umask",
		"# umask 22",
		"",
		"setenv  EDITOR  vi",
		"setenv  PAGER   more",
		"",
		"if ($?prompt) then",
		"		# An interactive shell -- set some stuff up",
		"		set prompt = \"%N@%m:%~ %# \"",
		"		set promptchars = \"%#\"",
		"",
		"		set filec",
		"		set history = 1000",
		"		set savehist = (1000 merge)",
		"		set autolist = ambiguous",
		"		# Use history to aid expansion",
		"		set autoexpand",
		"		set autorehash",
		"		set mail = (/var/mail/$USER)",
		"		if ( $?tcsh ) then",
		"				bindkey \"^W\" backward-delete-word",
		"				bindkey -k up history-search-backward",
		"				bindkey -k down history-search-forward",
		"		endif",
		"",
		"endif",
		"",
		"screenfetch"
	};
	std::string dir = this->mypasswd->pw_dir;
	std::string path = dir + "/.cshrc";
	remove(path.c_str());
	writeFile(path, lines);
	chown(path.c_str(), this->mypasswd->pw_uid, this->mypasswd->pw_gid);
}

void postinstall::genPlasma() {
	std::string lines[] {
		"#!/bin/sh",
		"/usr/local/bin/xterm -geometry +0+0 &",
		"KDE=/usr/local/bin/startkde",
		"test -x $KDE && exec /usr/local/bin/ck-launch-session $KDE",
		"exec /usr/local/bin/twm"

	};
	std::string dir = this->mypasswd->pw_dir;
	std::string path = dir + "/.xinitrc";
	writeFile(path, lines);
	chown(path.c_str(), this->mypasswd->pw_uid, this->mypasswd->pw_gid);
	addRepository();
}
void postinstall::addRepository() {
	std::string path1 = "/usr/local/etc/pkg";
	mkdir(path1.c_str(), 655);
	std::string path = "/usr/local/etc/pkg/repos";
	mkdir(path.c_str(), 655);
	std::string lines[] {
		"FreeBSD: {enabled: true}",
		" ",
		"Area51: {",
		"url: \"http://meatwad.mouf.net/rubick/poudriere/packages/110-amd64-kde/\",",
		"priority: 2,",
		"enabled: yes"
	};

	std::string of = path + "/Area51.conf";
	writeFile(of, lines);
	installPlasma();
}

void postinstall::installPlasma() {
	std::system("pkg update");
	std::system("pkg install konsole plasma5-plasma-desktop kde");

	std::fstream output("/etc/rc.conf", std::fstream::app);
	output << "sddm_enable=\"yes\"" << std::endl;
	output.close();
}
void postinstall::makeGermanKbd() {
    std::string lines[] {
        "Section \"InputClass\"",
        "   Identifier \"keyboard\"",
        "   MatchIsKeyboard \"yes\"",
        "   Option \"XkbLayout\" \"de\"",
        "   Option \"XkbVariant\" \"nodeadkeys\"",
        "EndSection"
    };
    std::string of = "/usr/local/etc/X11/xorg.conf.d/20-keyboard.conf";

    writeFile(of, lines);

}
template<size_t N>
void postinstall::writeFile(const std::string &path, const std::string (&lines)[N]) {
	std::fstream output(path.c_str(), std::fstream::app);
	for (auto& line: lines){
		output << line << std::endl;
	}
	output.close();
}
