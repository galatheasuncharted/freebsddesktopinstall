FreeBSDDesktopInstaller: main.o preinstall.o postinstall.o utils.o nvidia.o amd.o intel.o matrox.o installer.o
	clang++ -std=c++11 main.o preinstall.o postinstall.o utils.o nvidia.o amd.o intel.o matrox.o installer.o -o FreeBSDDesktopInstaller

main.o: main.cpp
	clang++ -std=c++11 -c main.cpp

preinstall.o: preinstall/preinstall.cpp preinstall/preinstall.hpp
	clang++ -std=c++11 -c preinstall/preinstall.cpp
	
postinstall.o: postinstall/postinstall.cpp postinstall/postinstall.hpp
	clang++ -std=c++11 -c postinstall/postinstall.cpp

utils.o: utils/utils.cpp utils/utils.hpp
	clang++ -std=c++11 -c utils/utils.cpp
	
nvidia.o: nvidia/nvidia.cpp nvidia/nvidia.hpp
	clang++ -std=c++11 -c nvidia/nvidia.cpp

amd.o: amd/amd.cpp amd/amd.hpp
	clang++ -std=c++11 -c amd/amd.cpp

intel.o: intel/intel.cpp intel/intel.hpp
	clang++ -std=c++11 -c intel/intel.cpp
	
matrox.o: matrox/matrox.cpp matrox/matrox.hpp
	clang++ -std=c++11 -c matrox/matrox.cpp

installer.o: installer/installer.cpp installer/installer.hpp
	clang++ -std=c++11 -c installer/installer.cpp
	
clean:
	rm *.o >> /dev/null