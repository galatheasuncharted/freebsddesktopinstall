/*
 * intel.hpp
 *
 *  Created on: 12.11.2017
 *      Author: gala
 */

#ifndef INTEL_INTEL_HPP_
#define INTEL_INTEL_HPP_

#include <iostream>

class intel {
public:
	intel();
	virtual ~intel();

    void createBootCfg();
    void createRC();
    void createXORG();
};

#endif /* INTEL_INTEL_HPP_ */
