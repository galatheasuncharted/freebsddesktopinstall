/*
 * intel.cpp
 *
 *  Created on: 12.11.2017
 *      Author: gala
 */

#include "intel.hpp"

intel::intel() {
	// TODO Auto-generated constructor stub

}

intel::~intel() {
	// TODO Auto-generated destructor stub
}
void intel::createBootCfg() {
    /**
     * kern.vty=vt
     * linux_enable="YES"
     */
    std::string cmd = "echo 'kern.vty=vt' >> /boot/loader.conf";
    std::system(cmd.c_str());
    cmd = "echo 'linux_enable=\"yes\"' >> /boot/loader.conf";
    std::system(cmd.c_str());
}
void intel::createRC() {
    /**
     * kld_list=""
     */
    //std::string cmd = "echo 'kld_list=\"\"' >> /etc/rc.conf";
    //std::system(cmd.c_str());
}
void intel::createXORG() {
    /**
     * Section "Device"
     *      Identifier  "Card0"
     *      Driver      "intel"
     * EndSection
     */
    std::string cmd = "mkdir -p /usr/local/etc/X11/xorg.conf.d/";
    std::system(cmd.c_str());
    cmd = "echo 'Section \"Device\"' >> /usr/local/etc/X11/xorg.conf.d/driver-intel.conf";
    std::system(cmd.c_str());
    cmd = "echo '      Identifier  \"Card0\"' >> /usr/local/etc/X11/xorg.conf.d/driver-intel.conf";
    std::system(cmd.c_str());
    cmd = "echo '      Driver      \"intel\"' >> /usr/local/etc/X11/xorg.conf.d/driver-intel.conf";
    std::system(cmd.c_str());
    cmd = "echo 'EndSection' >> /usr/local/etc/X11/xorg.conf.d/driver-intel.conf";
    std::system(cmd.c_str());
}

