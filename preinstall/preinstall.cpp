/*
 * preinstall.cpp
 *
 *   Created on: 25.10.2017
 *       Author: Galatheas Uncharted
 * Project Name: FreeBSDDesktop Installer
 * Copyright (C) 2017  FreeBSDTutorials.org & Galatheas Uncharted
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "preinstall.hpp"

preinstall::preinstall() {

	/**
	 * Init Desktop Vector
	 */
	//this->desktop_.push_back("");
	this->desktop_.push_back("XFCE4");				// OK
	this->desktop_.push_back("Gnome3");				// OK
	this->desktop_.push_back("KDE4 (english)");		// OK
	this->desktop_.push_back("Cinnamon (not supported yet)");			// currently not work todo writing xinitrc and xsession
	this->desktop_.push_back("Mate");				// OK
	this->desktop_.push_back("Budgie (currently not Supported)");		// currently no Budgie install Packages avail
	this->desktop_.push_back("BlackBox (currently not supported)");		// dose not start
	this->desktop_.push_back("LXDE");				// OK
	this->desktop_.push_back("KDE Plasma 5");		// OK

	/**
	* Init Display Manager Vector
	*/
	this->display_.push_back("lightdm lightdm-gtk-greeter");
	this->display_.push_back("gdm");
	this->display_.push_back("kdm");
	this->display_.push_back("slim");
	this->display_.push_back("xdm");




	/**
	 * Init Multimedia Vector
	 */
	this->multimedia_.push_back("VLC");
	this->multimedia_.push_back("Gimp");
	this->multimedia_.push_back("Clementine-Player");
	this->multimedia_.push_back("Asunder");
	this->multimedia_.push_back("Audacity");
	this->multimedia_.push_back("Krita");
	this->multimedia_.push_back("Shotcut");

	/**
	 * Init Office Vector
	 */
	this->office_.push_back("LibreOffice");
	this->office_.push_back("Thunderbird");
	this->office_.push_back("FileZilla");

	/**
	 * Init Graphic Vector
	 */
	this->graphic_.push_back("AMD");
	this->graphic_.push_back("Intel");
	this->graphic_.push_back("Nvidia");
	this->graphic_.push_back("VirtualBox-OSE-Additions");
	this->graphic_.push_back("Matrox");

	/**
	 * Init Browser Vector
	 */
	this->browser_.push_back("Chromium");
	this->browser_.push_back("Firefox");
	this->browser_.push_back("Opera");

	/**
	 * Init SysUtils Vector
	 */
	this->sysutils_.push_back("zfsnap2");
	this->sysutils_.push_back("OctoPKG");
	this->sysutils_.push_back("gksu");
	this->sysutils_.push_back("sudo");
	this->sysutils_.push_back("Nano");
	this->sysutils_.push_back("Screenfetch");

}

preinstall::~preinstall() {

}

bool preinstall::installBrowser() {
	return this->b_browser_;
}
bool preinstall::installGraphic() {
	return this->b_graphic_;
}
bool preinstall::installMultimedia() {
	return this->b_multimedia_;
}
bool preinstall::installOffice() {
	return this->b_office_;
}
bool preinstall::installSysUtils() {
	return this->b_sysutils_;
}

void preinstall::setBrowser(bool b) {
	this->b_browser_ = b;
}
void preinstall::setGraphic(bool b) {
	this->b_graphic_ = b;
}
void preinstall::setMultimedia(bool b) {
	this->b_multimedia_ = b;
}
void preinstall::setOffice(bool b) {
	this->b_office_ = b;
}
void preinstall::setSysUtils(bool b) {
	this->b_sysutils_ = b;
}

std::string preinstall::getDesktopName(int &number) {

	return this->desktop_.at(number -1);

}
std::string preinstall::getDisplayName(int &number) {

	return this->display_.at(number -1);
}
std::string preinstall::getBrowserName(int &number) {

	return this->browser_.at(number -1);

}
std::string preinstall::getGraphicName(int &number) {

	return this->graphic_.at(number -1);

}

std::vector<std::string> preinstall::getBrowser() {
	return this->browser_;
}
std::vector<std::string> preinstall::getDesktop() {
	return this->desktop_;
}
std::vector<std::string> preinstall::getDisplay() {
	return this->display_;
}
std::vector<std::string> preinstall::getGraphic() {
	return this->graphic_;
}
std::vector<std::string> preinstall::getMultimedia() {
	return this->multimedia_;
}
std::vector<std::string> preinstall::getOffice() {
	return this->office_;
}
std::vector<std::string> preinstall::getSysutils() {
	return this->sysutils_;
}

std::string preinstall::getDesktopPackage(int &number) {

	std::string rvalue;

	switch(number) {
	case 1:
		/**
		 * xfce4 packages
		 */
		rvalue = "xfce xfce4-session xfce4-goodies gnome-icons-elementary gtk-arc-themes";
		break;
	case 2:
		/**
		 * Gnome3 packages
		 */
		rvalue = "gnome3";
		break;
	case 3:
		/**
		 * KDE/Plasma
		 */
		rvalue = "x11/kde4 german/kde4-l10n";
		break;
	case 4:
		/**
		 * Cinnamon
		 */
		rvalue = "cinnamon cinnamon-session";
		break;
	case 5:
		/**
		 * Mate
		 */
		rvalue = "mate mate-desktop mate-base mate-session-manager gnome-keyring gnome-icon-theme gnome-icons-elementary gtk-arc-themes";
		break;
	case 6:
		rvalue = "";
		break;
	case 7:
		rvalue = "blackbox";
		break;
	case 8:
		rvalue = "lxde-common lxde-meta lxde-icon-theme";
		break;
	case 9:
		rvalue = "git bash gmake cmake pkgconf gettext-tools binutils xterm twm sddm";
		break;
	}

	return rvalue;
}

void preinstall::addRepository() {
	std::string path = "/usr/local/etc/pkg/repos";
	std::string cmd = "mkdir -p " + path;
	std::system(cmd.c_str());
	std::string lines[] {
		"FreeBSD: {enabled: true}",
		" ",
		"Area51: {",
		"url: \"http://meatwad.mouf.net/rubick/poudriere/packages/110-amd64-kde/\",",
		"priority: 2,",
		"enabled: yes"
	};

	std::string of = path + "/Area51.conf";

	std::fstream output1(of.c_str(), std::fstream::app);
	for (auto& line: lines){
		output1 << line << std::endl;
	}
}
