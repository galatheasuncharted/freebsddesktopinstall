/*
 * preinstall.hpp
 *
 *   Created on: 25.10.2017
  *       Author: Galatheas Uncharted
 * Project Name: FreeBSDDesktop Installer
 * Copyright (C) 2017  FreeBSDTutorials.org & Galatheas Uncharted
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PREINSTALL_PREINSTALL_HPP_
#define PREINSTALL_PREINSTALL_HPP_

#include <vector>
#include <string>
#include <fstream>


class preinstall {
public:
	preinstall();
	virtual ~preinstall();

	bool installBrowser();
	bool installGraphic();
	bool installMultimedia();
	bool installOffice();
	bool installSysUtils();

	void setBrowser(bool b);
	void setGraphic(bool b);
	void setMultimedia(bool b);
	void setOffice(bool b);
	void setSysUtils(bool b);

	void addRepository();

	std::string getDesktopName(int &number);
	std::string getDisplayName(int &number);
	std::string getBrowserName(int &number);
	std::string getGraphicName(int &number);

	std::vector<std::string> getBrowser();
	std::vector<std::string> getDesktop();
	std::vector<std::string> getDisplay();
	std::vector<std::string> getGraphic();
	std::vector<std::string> getMultimedia();
	std::vector<std::string> getOffice();
	std::vector<std::string> getSysutils();
	std::string getDesktopPackage(int &number);


private:
	bool b_browser_ = false;
	bool b_multimedia_= false;
	bool b_graphic_ = false;
	bool b_office_ = false;
	bool b_sysutils_ = false;

	std::vector<std::string> browser_;
	std::vector<std::string> desktop_;
	std::vector<std::string> display_;
	std::vector<std::string> graphic_;
	std::vector<std::string> multimedia_;
	std::vector<std::string> office_;
	std::vector<std::string> sysutils_;
};

#endif /* PREINSTALL_PREINSTALL_HPP_ */
